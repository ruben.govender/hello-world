//
//  Hello_WorldApp.swift
//  Hello World
//
//  Created by Ruben Govender on 4/8/22.
//

import SwiftUI

@main
struct Hello_WorldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
