//
//  ContentView.swift
//  Hello World
//
//  Created by Ruben Govender on 4/8/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world! - My test application - Third update")
            .foregroundColor(Color.purple)
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
